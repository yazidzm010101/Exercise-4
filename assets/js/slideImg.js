var indeksSlide = 0;

function onLoad(){
    var kontainerString = "";
    var kontainerHTML = document.getElementsByClassName('kontainerGambar')[0];
    arraySlide = ["assets/img/Gundars1.jpg",
                        "assets/img/Gundars2.jpg",
                        "assets/img/Gundars3.jpg",];
    for(x = 0;x<arraySlide.length;x++){
        kontainerString += 
        '<div class="gambarAktif fade" style="background-image:url('+arraySlide[x]+');">'
            +'<div class="kontenGambar">'
                +'Selamat Datang di LePKom Gunadarma'
            +'</div>'
        +'</div>';
    }
    kontainerString +=
        // '<a class="mundur" onclick="slideNav(-1)"></a>'
        // +'<a class="maju" onclick="slideNav(1)"></a>'
        '<div class="dotKontainer">';        
    for(x = 0;x<arraySlide.length;x++){
        kontainerString +=
        '<a class="dotSlide" onclick="setSlide('
        +(x)
        +')">■</a>';
    }                  
    kontainerString += '</div>';
    // ■
    kontainerHTML.innerHTML = kontainerString;
    tampilSlide(indeksSlide);
    timeEv();
}

function timeEv(){
    window.setTimeout(function(){
        slideNav(1);
        timeEv();
    }, 7000);
}
function slideNav(x){
    indeksSlide += x
    tampilSlide(indeksSlide);
}
function setSlide(x){
    indeksSlide = x
    tampilSlide(indeksSlide);
}
function tampilSlide(x){
    var y;
    var gambars = document.getElementsByClassName("gambarAktif");
    var dots = document.getElementsByClassName("dotSlide");
    if (x > (gambars.length - 1)){
        
        indeksSlide = 0;
    }
    if (x < 0){
        indeksSlide = (gambars.length)-1;
    }
    for(y = 0; y < gambars.length; y++){
        gambars[y].style.display = "none";
        dots[y].style.color = "black";
    }
    gambars[indeksSlide].style.display = "block";
    dots[indeksSlide].style.color = "white";
}